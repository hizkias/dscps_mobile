import 'package:flutter/material.dart';


class UiHelper {
  static Widget createComponent(Widget home){
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.teal,
        accentColor: Colors.amber
      ),
      home: home
    );
  }
  static Widget trafficUserAppBar(context){
    return AppBar(
      leading: IconButton(icon: Icon(Icons.menu), onPressed: (){
        Scaffold.of(context).openDrawer();
//        Scaffold.of(context)
      }),
      title: Text('DSCPS'),
      centerTitle: true,
    );
  }

  static Widget trafficSkeleton(){

  }

}