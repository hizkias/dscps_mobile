import 'package:dscps/models/License.dart';
import 'package:dscps/models/User.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class DatabaseHelper {
  static DatabaseHelper _databaseHelper;
  static Database _database;
  DatabaseHelper._createInstance();

  factory DatabaseHelper() {
    if (_databaseHelper == null) {
      _databaseHelper = DatabaseHelper._createInstance();
    }
    return _databaseHelper;
  }
  Future<Database> get database async {
    if (_database == null) {
      _database = await this._initializeDatabase();
    }
    return _database;
  }

  Future<Database> _initializeDatabase() async {
    var dbPath = await getApplicationDocumentsDirectory();
    var conString = dbPath.path + 'notes.db';

    var db =
        await openDatabase(conString, version: 1, onCreate: _createDatabase);

    return db;
  }

  void _createDatabase(Database db, int version) async {
    await db.execute(
        'CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT, accessToken TEXT, userId TEXT, username TEXT, firstname TEXT, middlename TEXT, lastname TEXT, email TEXT, userType TEXT, driverLicenseId TEXT, speedZoneId TEXT)');
    await db.execute(
        'CREATE TABLE licenses(id INTEGER PRIMARY KEY AUTOINCREMENT, firstName TEXT, middleName TEXT, lastName TEXT, gender TEXT, age INTEGER, bloodGroup TEXT, licenseNo TEXT, licenseCategory TEXT, licenseSite TEXT, town TEXT, imageUrl TEXT)');
  }

  Future<int> addNote(Map<String, dynamic> note) async {
    var db = await this.database;
    var result = await db.insert('notes', note);
    return result;
  }

  Future<List<Map<String, dynamic>>> getAllNotes() async {
    var db = await this.database;
    var result = await db.query('notes', orderBy: 'id DESC');

    return result;
  }

  Future<Map<String, dynamic>> getANote(int id) async {
    var db = await this.database;
    var result = await db.query('notes', where: 'id = ?', whereArgs: [id]);

    return result.first;
  }

  Future<int> editNote(int id, Map<String, dynamic> data) async {
    var db = await this.database;
    var result =
        await db.update('notes', data, where: 'id = ?', whereArgs: [id]);
    return result;
  }

  Future<int> deleteNote(id) async {
    var db = await this.database;
    var result = await db.delete('notes', where: 'id = ?', whereArgs: [id]);
    return result;
  }

  Future<User> getUser() async {
    var db = await this.database;
    var result = await db.query('users');
    if (result.length > 0) {
      return User.fromJson(result.first, false);
    } else {
      return null;
    }
  }

  Future<int> addUser(user) async {
    var db = await this.database;
    var result = await db.insert('users', user);
    return result;
  }

  Future<int> deleteUsers() async {
    var db = await this.database;
    var result = await db.delete('users');
    return result;
  }


  Future<int> addDriverLicense(license) async{
    print('about fo add license');
    var db = await this.database;
    var result = await db.insert('licenses', license);
    return result;
  }

  Future<License> getDriverLicense() async{
    var db = await this.database;
    var result = await db.query('licenses');
    if(result.length > 0){
      return License.fromJson(result.first);
    }

    return null;
  }

  Future<int> deleteLicenses() async {
    var db = await this.database;
    return db.delete('licenses');
  }
}
