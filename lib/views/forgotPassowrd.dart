import 'package:dscps/services/apiService.dart';
import 'package:flutter/material.dart';
import 'package:dscps/utils/uiHelper.dart';

class ForgotPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text('Forgot passowrd'),
      ),
      body: _ForgotPasswordForm(),
    ));
  }
}

class _ForgotPasswordForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ForgotPassowrdFormState();
  }
}

class _ForgotPassowrdFormState extends State<_ForgotPasswordForm> {
  GlobalKey<FormState> _form = new GlobalKey<FormState>();
  TextEditingController emailController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
      key: _form,
      child: ListView(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.email),
            title: TextFormField(
                controller: emailController,
                decoration: InputDecoration(hintText: 'Enter reset email'),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Reset email is required';
                  }
                }),
          ),
          ListTile(
            title: RaisedButton(
              onPressed: () async{
                if(_form.currentState.validate()){
                  var response = await ApiService.resetEmail({
                    'email': '${emailController.text}'
                  });

                  if(response.statusCode == 204){
                    Scaffold.of(context).showSnackBar(SnackBar(content: Text('Succefully sent reset email link check your email')));
                  }
                  else if(response.statusCode == 404){

                    Scaffold.of(context).showSnackBar(SnackBar(content: Text('The email you sent is not associated with any account')));
                  }
                  else {

                    Scaffold.of(context).showSnackBar(SnackBar(content: Text('Error while sending reset email')));
                  }
                }
              },
              child: Text('Submit'),
            ),
          )
        ],
      ),
    );
  }
}
