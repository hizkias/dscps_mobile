import 'package:dscps/models/PenalityRecord.dart';
import 'package:dscps/views/driverUser/recordDetail.dart';
import 'package:flutter/material.dart';
import '../../utils/uiHelper.dart';
import '../../services/apiService.dart';

class MyRecords extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text('My Records'),
      ),
      body: _MyRecordsList(),
    ));
  }
}

class _MyRecordsList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyRecordsListState();
  }
}

class _MyRecordsListState extends State<_MyRecordsList> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder<List<PenalityRecord>>(
        future: ApiService.getMyPenalityRecords(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            print('has data: ${snapshot.data.length}');
            if (snapshot.data.length == 0) {
              return Center(
                child: Text('No records!'),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    var record = snapshot.data[index];

                    return ListTile(
                      leading: CircleAvatar(
                        child: Icon(
                            record.isResolved ? Icons.done_all : Icons.warning),
                      ),
                      title: Text('${record.crimeType.name}'),
                      subtitle: Text(
                          'Fine amount: ETB ${record.crimeType.fineAmount}'),
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    PenalityRecordDetail(record)));
                      },
                    );
                  });
            }
          } else if (snapshot.hasError) {
            print('The error is ${snapshot.error.toString()}');
            return Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    child: Icon(Icons.warning),
                  ),
                  Container(
                    child: Text(
                      'Error while getting list...',
                      style: TextStyle(fontSize: 25),
                    ),
                  )
                ],
              ),
            );
          }

          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}
