import 'package:dscps/models/License.dart';
import 'package:dscps/services/apiService.dart';
import 'package:dscps/utils/dbHelper.dart';
import 'package:flutter/material.dart';
import '../../utils/uiHelper.dart';

class MyLicense extends StatelessWidget {
  DatabaseHelper _dbHelper = DatabaseHelper();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text('My license'),
      ),
      body: FutureBuilder<License>(
          future: _dbHelper.getDriverLicense(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              License license = snapshot.data;

              return ListView(
                children: <Widget>[
                  Card(
                    child: ListTile(
                      title: FadeInImage.assetNetwork(
                        width: MediaQuery.of(context).size.width / 5,
                        height:
                        MediaQuery.of(context).size.height / 5,
                        placeholder: 'assets/spinningwheel.gif',
                        image:
                        '${ApiService.licensepi}Containers/files/download/${license.imageUrl}',
                      ),
                    ),
                  ),
                  Card(
                    child: ListTile(
                      leading: Icon(Icons.person),
                      title:  Text('Full name'),
                      subtitle: Text('${license.getFullName()}'),
//                      title: Row(
//
//                        children: <Widget>[
//                          Text('First name'),
//                          Text('${license.firstName}')
//                        ],
//                      ),
                    ),
                  ),

                  Card(
                    child: ListTile(
                      leading: Icon(Icons.wc),
                      title:  Text('Gender'),
                      subtitle: Text('${license.gender}'),
//                      title: Row(
//
//                        children: <Widget>[
//                          Text('First name'),
//                          Text('${license.firstName}')
//                        ],
//                      ),
                    ),
                  ),
                  Card(
                    child: ListTile(
                      leading: Icon(Icons.history),
                      title:  Text('Age'),
                      subtitle: Text('${license.age}'),
//                      title: Row(
//
//                        children: <Widget>[
//                          Text('First name'),
//                          Text('${license.firstName}')
//                        ],
//                      ),
                    ),
                  ),
                  Card(
                    child: ListTile(
                      leading: Icon(Icons.group_work),
                      title:  Text('Blood group'),
                      subtitle: Text('${license.bloodGroup}'),
//                      title: Row(
//
//                        children: <Widget>[
//                          Text('First name'),
//                          Text('${license.firstName}')
//                        ],
//                      ),
                    ),
                  )
                ],
              );
            }
            return CircularProgressIndicator();
          }),
    ));
  }
}
