import 'package:dscps/models/PenalityRecord.dart';
import 'package:flutter/material.dart';
import 'package:dscps/utils/uiHelper.dart';

import 'package:dscps/services/apiService.dart';

class PenaltiesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text('Records by me'),
      ),
      body: FutureBuilder<List<PenalityRecord>>(
          future: ApiService.getPenalties(), builder: (context, snapshot) {
            if(snapshot.hasData){
              if(snapshot.data.length == 0){
                return Center(
                  child: Text('No penalities recorded by you'),
                );
              }
              else {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index){
                      PenalityRecord record = snapshot.data[index];
                      return ListTile(
                        title: Text('${record.driverLicenseId}'),
                        subtitle: Text('${record.crimeType.name}'),
                        trailing: Text('ETB ${record.crimeType.fineAmount}'),
                      );
                    }
                );
              }
            }
            else if(snapshot.hasError){
              return Center(
                child: Text('Error while getting penalities list'),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
      }),
    ));
  }
}
