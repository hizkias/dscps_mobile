import 'package:dscps/models/License.dart';
import 'package:dscps/models/User.dart';
import 'package:dscps/services/apiService.dart';
import 'package:dscps/utils/dbHelper.dart';
import 'package:dscps/utils/uiHelper.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';
import 'dart:io';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';

import '../login.dart';
import 'myLicense.dart';
import 'myRecords.dart';

class DriverHome extends StatelessWidget {
  User user;
  License license;
  DatabaseHelper _dbHelper = DatabaseHelper();

  DriverHome(this.user, this.license) {}
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: _ScaffoldLeading(),
        title: Text('DSCPS'),
        centerTitle: true,
      ),
      drawer: _DrawerComponent(user, license),
      body: QrGenerator(this.user),
    ));
  }

  static void toggleDrawer(context) {
    Scaffold.of(context).openDrawer();
  }
}

class _DrawerComponent extends StatelessWidget {
  User user;
  License license;
  _DrawerComponent(this.user, this.license) {}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            currentAccountPicture: CircleAvatar(
              child: Text(
                  '${license.firstName[0].toUpperCase()}${license.middleName[0].toUpperCase()}'),
            ),
            accountEmail: Text('${user.email}'),
            accountName: Text('${license.firstName} ${license.middleName}'),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DriverHome(this.user, this.license)));
            },
          ),
          ListTile(
            leading: Icon(Icons.insert_drive_file),
            title: Text('My license'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyLicense()));
            },
          ),
          ListTile(
            leading: Icon(Icons.history),
            title: Text('My records'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => MyRecords()));
            },
          ),
//          ListTile(
//            leading: Icon(Icons.settings),
//            title: Text('Settings'),
//            onTap: () {
//              Navigator.push(context,
//                  MaterialPageRoute(builder: (context) => Settings()));
//            },
//          ),
//          ListTile(
//            leading: Icon(Icons.scanner),
//            title: Text('Scan license'),
//            onTap: () {
//              Navigator.push(context,
//                  MaterialPageRoute(builder: (context) => ScanLicense()));
//            },
//          ),
          ListTile(
            leading: Icon(Icons.lock),
            title: Text('Logout'),
            onTap: () async {
              var logoutResponse = await ApiService.processLogout();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginComponent()));
            },
          ),
        ],
      ),
    );
  }
}

class _ScaffoldLeading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.menu),
      onPressed: () {
        DriverHome.toggleDrawer(context);
      },
    );
  }
}

class QrGenerator extends StatefulWidget {
  User user;
  QrGenerator(this.user) {}

  @override
  State<StatefulWidget> createState() => QrGeneratorState(this.user);
}

class QrGeneratorState extends State<QrGenerator> {
  static const double _topSectionTopPadding = 10.0;
  static const double _topSectionBottomPadding = 0.0;
  static const double _topSectionHeight = 50.0;
  DatabaseHelper _dbHelper = DatabaseHelper();
  User user;
  GlobalKey globalKey = new GlobalKey();
  String _dataString = "Hello from this QR";
  String _inputErrorText;
  final TextEditingController _textController = TextEditingController();

  QrGeneratorState(this.user) {}
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<License>(
      future: _dbHelper.getDriverLicense(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return _contentWidget(snapshot.data);
        } else if (snapshot.hasError) {
          return Center(
            child: Text('Error while getting driver license'),
          );
        }
        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

  _contentWidget(License license) {
    final bodyHeight = MediaQuery.of(context).size.height -
        MediaQuery.of(context).viewInsets.bottom;
    return Container(
      color: const Color(0xFFFFFFFF),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
              top: _topSectionTopPadding,
              left: 20.0,
              right: 10.0,
              bottom: _topSectionBottomPadding,
            ),
            child: Container(
              height: _topSectionHeight,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Expanded(
                    child: ListTile(
                      leading: Icon(
                        Icons.person,
                        size: 60,
                      ),
                      title: Text(
                        '${license.firstName} ${license.middleName}',
//                        style: TextStyle(fontSize: 25),
                      ),
                      subtitle: Text('Licensed driver'),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: Center(
              child: RepaintBoundary(
                key: globalKey,
                child: QrImage(
                  data: license.licenseNo,
                  size: 0.5 * bodyHeight,
                  onError: (ex) {
                    print("[QR] ERROR - $ex");
                    setState(() {
                      _inputErrorText =
                          "Error! Maybe your input value is too long?";
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
