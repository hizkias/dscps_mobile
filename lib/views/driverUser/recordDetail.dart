import 'package:dscps/models/PenalityRecord.dart';
import 'package:flutter/material.dart';
import 'package:dscps/utils/uiHelper.dart';


class PenalityRecordDetail extends StatelessWidget {
  PenalityRecord penalityRecord;
  PenalityRecordDetail(this.penalityRecord);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(
      Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: (){
                Navigator.pop(context);
              }),
          title: Text('Penality detail'),
        ),
        body: ListView(
          children: <Widget>[
            Card(
              child: ListTile(
                leading: Icon(Icons.refresh),
                title: Text('Reference code'),
                subtitle: Text('${penalityRecord.referenceNumber}'),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.warning),
                title: Text('Crime type'),
                subtitle: Text('${penalityRecord.crimeType.name}'),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.note),
                title: Text('Note by traffic officer'),
                subtitle: Text('${penalityRecord.noteOnIncidence}'),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.traffic),
                title: Text('Traffic police'),
                subtitle: Text('${penalityRecord.trafficFullname}'),
              ),
            ),
            Card(
              child: ListTile(
                leading: Icon(Icons.timer),
                title: Text('Time of violtion'),
                subtitle: Text('${penalityRecord.timeOfIncidence.toLocal().toString()}'),
              ),
            ),
          ],
        ),
      )
    );
  }

}