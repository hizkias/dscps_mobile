import 'package:dscps/models/License.dart';
import 'package:dscps/models/User.dart';
import 'package:flutter/material.dart';
import '../utils/uiHelper.dart';

import './trafficUser/home.dart';
import './driverUser/home.dart';
import '../services/apiService.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../utils/dbHelper.dart';
import 'forgotPassowrd.dart';

class LoginComponent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(title: Text('DSCPS'), centerTitle: true),
      body: _LoginComponentForm(),
    ));
  }
}

class _LoginComponentForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginComponentState();
  }
}

class _LoginComponentState extends State<_LoginComponentForm> {
  final _formKey = GlobalKey<FormState>();
  final _dbHelper = new DatabaseHelper();

  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();
  var isLoginButtonClicked = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(12),
          child: ListView(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Login',
                    style: TextStyle(fontSize: 18.0),
                  )
                ],
              ),
              Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                child: TextFormField(
                  controller: _usernameController,
                  decoration: InputDecoration(
                    icon: Icon(Icons.person_outline),
                    hintText: 'username',
                  ),
//                  controller: _usernameController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'username is required';
                    }
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                child: TextFormField(
                  controller: _passwordController,
                  decoration: InputDecoration(
                    icon: Icon(Icons.lock),
                    hintText: 'password',
                  ),
//                  controller: _passwordController,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'password is required';
                    }
                  },
                  obscureText: true,
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 8, bottom: 8),
                child: RaisedButton.icon(
                    onPressed: () async {
                            if (_formKey.currentState.validate()) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                  content:
                                      Text('Signing into your account...')));

                              setState(() {
                                this.isLoginButtonClicked = true;
                              });
                              try {
                                User user =
                                    await ApiService.processLogin({
                                  'username': '${_usernameController.text}',
                                  'password': '${_passwordController.text}'
                                });
                                print('In the login');
                                print(user.toMap());
                                await _dbHelper.addUser(user.toMap());

                               if(user.userType == 'Traffic_police'){
                                 Navigator.push(context, MaterialPageRoute(builder: (context) => TrafficHome(user)));
                               }
                               else if(user.userType == 'Driver') {
                                 print('about to get driver license');
                                 License license = await ApiService.getDriverLicense(user.driverLicenseId);
                                  await _dbHelper.addDriverLicense(license.toMap());
                                  print('added license');
                                 Navigator.push(context, MaterialPageRoute(builder: (context) => DriverHome(user, license)));
                               }
                                else {
                                 _dbHelper.deleteUsers();
                                 Scaffold.of(context).showSnackBar(SnackBar(content: Text('Invalid username or password!')));
                               }
                              }
                              on Exception catch(exception){
                                setState(() {
                                  this.isLoginButtonClicked = false;
                                });
                               if(exception.runtimeType.toString() == '_Exception'){
                                 Scaffold.of(context).showSnackBar(SnackBar(content: Text('Incorrect username of password error!')));
                               }
                               else if(exception.runtimeType.toString() == 'SocketException'){
                                 Scaffold.of(context).showSnackBar(SnackBar(content: Text('Unable to locate server!')));
                               }
                              }
                              // ignore: non_type_in_catch_clause
                            }
                          }
                       ,
                    icon: Icon(Icons.forward),
                    label: Text('Login')),
              ),
              Container(
                margin: EdgeInsets.only(top: 0, bottom: 8),
                child: FlatButton(onPressed: (){
                  Navigator.push(context, MaterialPageRoute(builder: (context) => ForgotPassword()));
                }, child: Text('Forgot passowrd'),),
              )
            ],
          ),
        ));
  }
}
