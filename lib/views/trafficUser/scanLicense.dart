import 'package:dscps/models/License.dart';
import 'package:dscps/views/trafficUser/showLicense.dart';
import 'package:flutter/material.dart';
import '../../utils/uiHelper.dart';

import 'dart:async';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import '../../services/apiService.dart';

class ScanLicense extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text('Scan license'),
      ),
      body: ScanLicenseBody(),
    ));
  }
}

class ScanLicenseBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ScanLicenseBodyState();
  }
}

class _ScanLicenseBodyState extends State<ScanLicenseBody> {
  String licenseId = '';
  var ctx;
  @override
  Widget build(BuildContext context) {
    this.ctx = context;
    // TODO: implement build
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          IconButton(icon: Icon(Icons.camera_alt), onPressed: () {}),
          RaisedButton.icon(
              onPressed: scan, icon: Icon(Icons.scanner), label: Text('Scan')),
        ],
      ),
    );
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() => this.licenseId = barcode);
      Navigator.push(context, MaterialPageRoute(builder: (context) => ShowLicense(this.licenseId)));
      print('The result is ${this.licenseId}');
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.licenseId = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.licenseId = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => this.licenseId =
          'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.licenseId = 'Unknown error: $e');
    }
  }
}
