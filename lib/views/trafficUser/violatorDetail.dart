import 'package:dscps/models/SpeedDetectorDevice.dart';
import 'package:dscps/views/trafficUser/penalizeDriver.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:dscps/utils/uiHelper.dart';
import 'package:latlong/latlong.dart';
import 'dart:isolate';
import 'dart:io';
import 'package:dscps/services/apiService.dart';

class ViolatorDetail extends StatefulWidget {
  var deviceId;

  ViolatorDetail(this.deviceId) {}
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ViolatorDetailState(this.deviceId);
  }
}

class _ViolatorDetailState extends State<ViolatorDetail> {
  SpeedDetectorDevice violator;
  MapController mapController = MapController();
  Isolate isolate;
  bool isTracking = false;
  dynamic currentZoom = 14.0;
  static dynamic violatorId;
  dynamic deviceId;
  SendPort sendPort;
  _ViolatorDetailState(this.deviceId) {
//    violatorId = this.violator.id;
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder<SpeedDetectorDevice>(
      future: ApiService.getSpeedViolator(this.deviceId),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          this.violator = snapshot.data;

          return UiHelper.createComponent(Scaffold(
              appBar: AppBar(
                  leading: IconButton(
                    icon: Icon(Icons.arrow_back),
                    onPressed: () {
                      if (isolate != null) {
                        this.stopTracking();
                      }
                      Navigator.pop(context);
                    },
                  ),
                  actions: <Widget>[
                    FlatButton(
                      child: Text(
                        '${violator.vehicle.plateNo}',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () {},
                    )
                  ],
                  title: Text(
                    'Violator detail',
                  )),
              body: FlutterMap(
                  mapController: mapController,
                  options: new MapOptions(
                      center: new LatLng(violator.lat, violator.lng),
                      minZoom: 5.0,
                      zoom: currentZoom),
                  layers: [
                    TileLayerOptions(
                        urlTemplate:
                            'https://api.mapbox.com/styles/v1/hizkias/cjvtw3z5n0au11clvgjrtpybt/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiaGl6a2lhcyIsImEiOiJjanYzb2dweWcyaHFoM3lsaml0a3lhcmhzIn0.nG_CTyu-otJU2VC5cnZ-ZQ',
                        additionalOptions: {
                          'accessToken':
                              'pk.eyJ1IjoiaGl6a2lhcyIsImEiOiJjanYzb2dweWcyaHFoM3lsaml0a3lhcmhzIn0.nG_CTyu-otJU2VC5cnZ-ZQ',
                          'id': 'mapbox.mapbox-streets-v8'
                        }),
                    new MarkerLayerOptions(
                        markers: this.markPreviousViolationSpots()),
                    new MarkerLayerOptions(markers: [
                      new Marker(
                          width: 45.0,
                          height: 45.0,
                          point: new LatLng(violator.lat, violator.lng),
                          builder: (context) => new Container(
                                child: IconButton(
                                  icon: Icon(Icons.location_on),
                                  color: Colors.blue,
                                  iconSize: 45.0,
                                  tooltip: 'Working',
                                  onPressed: () {
                                  },
                                ),
                              ))
                    ])
                  ]),
              floatingActionButton: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Container(
//              margin: EdgeInsets.only(left: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          RaisedButton.icon(
                            icon: Icon(
                              Icons.my_location,
                              color: isTracking ? Colors.green : Colors.black,
                            ),
                            label: Text(
                              'Track',
//                        style: TextStyle(color: isTracking ? Colors.green: Colors.black),
                            ),
                            onPressed: isTracking
                                ? () {
                                    this.stopTracking();
                                  }
                                : () {
                                    this.startTracking();
                                  },
                          ),
//                VerticalDivider(),
                          RaisedButton.icon(
                            icon: Icon(Icons.history),
                            label: Text('Violations'),
                            onPressed: () {
                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    List<Widget> violators = new List<Widget>();
                                    for (var i = 0;
                                        i < violator.speedViolations.length;
                                        i++) {
                                      violators.add(ListTile(
                                        leading: Icon(Icons.location_on),
                                        title: Text(
                                            '${violator.speedViolations[i].speedZone.name}'),
                                        subtitle: Text(
                                          'Was driving at ${violator.speedViolations[i].speedDetected} km/hr while the limit was ${violator.speedViolations[i].speedZone.speedLimit} km/hr',
                                        ),
                                        trailing: Text(
                                            '${violator.speedViolations[i].timeOfViolation.difference(DateTime.now()).inHours * -1}hrs ago'),
                                        onTap: () {
                                          mapController.move(
                                              LatLng(
                                                  violator
                                                      .speedViolations[i].lat,
                                                  violator
                                                      .speedViolations[i].lng),
                                              18);
                                        },
                                      ));
                                    }
                                    return ListView(children: violators);
                                  });
                            },
                          ),
                          RaisedButton.icon(
                            icon: Icon(Icons.warning),
                            label: Text('Penalize'),
                            onPressed: () async {
                              var crimeTypes = await ApiService.getCrimeTypes();
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => PenalizeDriver
                                          .fromViolatorDetailScreen(violator)));
                            },
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              )));
        }
        return Center(
          child: UiHelper.createComponent(Scaffold(
            appBar: AppBar(
              leading: IconButton(
                  icon: Icon(Icons.arrow_back),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              title: Text('Violator detail'),
            ),
            body: Center(
              child: CircularProgressIndicator(),
            ),
          )),
        );
      },
    );
  }

// marks previously violated spots
  List<Marker> markPreviousViolationSpots() {
    List<Marker> previosSpots = List<Marker>();

    for (var i = 0; i < violator.speedViolations.length; i++) {
      previosSpots.add(Marker(
          width: 45.0,
          height: 45.0,
          point: new LatLng(
              violator.speedViolations[i].lat, violator.speedViolations[i].lng),
          builder: (context) => new Container(
                child: IconButton(
                  icon: Icon(Icons.warning),
                  color: Colors.red,
                  iconSize: 45.0,
                  onPressed: () {
                    print('Marker tapped');
                    mapController.move(
                        LatLng(violator.speedViolations[i].lat,
                            violator.speedViolations[i].lng),
                        18);
                  },
                ),
              )));
    }

//    print(previosSpots);

    return previosSpots;
  }

  // start tracking the violated vehicle(creates a new isolate/thread)
  void startTracking() async {
    var receiver = ReceivePort();
    isolate = await Isolate.spawn(_doTheActualTracking, receiver.sendPort);

//    Isolate.spa
    setState(() {
      isTracking = true;
    });
    receiver.listen((data) {
      if (data is SendPort) {
        data.send(violator.id);
      } else {
        setState(() {
          this.violator = data;
        });
//        mapController.move(LatLng(data.lat, data.lng), mapController.zoom);
      }
    });
  }

  // stop tracking
  void stopTracking() {
    if (isolate != null) {
      isolate.kill(priority: Isolate.immediate);
      isolate = null;
      setState(() {
        isTracking = false;
      });
    }
  }

  static void setViolatorId(id) {
    _ViolatorDetailState.violatorId = id;
  }
}

void _doTheActualTracking(SendPort sendPort) async {
  var receiverPort = ReceivePort();
  sendPort.send(receiverPort.sendPort);
  receiverPort.listen((data) async {
    while (true) {
      sleep(Duration(seconds: 2));
      var _violator = await _getViolator(data);
      sendPort.send(_violator);
    }
  });

//
}

Future<SpeedDetectorDevice> _getViolator(id) async {
  var violatorDevice = await ApiService.getSpeedViolator(id);
  if (violatorDevice == null) {
    _getViolator(id);
  } else {
    return violatorDevice;
  }
}
