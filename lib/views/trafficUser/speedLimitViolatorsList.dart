import 'package:dscps/models/SpeedLimitViolator.dart';
import 'package:dscps/models/Vehicle.dart';
import 'package:flutter/material.dart';
import '../../utils/uiHelper.dart';

import './violatorDetail.dart';
import './penalizeDriver.dart';
import '../../services/apiService.dart';

class SpeedViolators extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text('Speed violators'),
      ),
      body: _SpeedViolatorsBody(),
    ));
  }
}

class _SpeedViolatorsBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SpeedViolatorsBodyState();
  }
}

class _SpeedViolatorsBodyState extends State<_SpeedViolatorsBody> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder<List<Vehicle>>(
        future: ApiService.getSpeedLimitViolatiors(),
        builder: (context, snapshot) {
//          print(snapshot.data);
          if (snapshot.hasData) {
            print('Yes it has data');
            if (snapshot.data.length == 0) {
              return Center(
                child: Text('No speed limit violators!'),
              );
            }
           else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (context, index) {
                    Vehicle vehicle = snapshot.data[index];
                    return ListTile(
                      leading: CircleAvatar(
                        child: Icon(Icons.directions_car),
                      ),
                      title: Text('Plate no: ${vehicle.plateNo}'),
                      subtitle: Text('Owner: ${vehicle.owner.name}'),
                      onTap: () async {
                        print('The vehicle id ${vehicle.deviceId}');
//                     print('The violator id here inside ${vehicle.deviceId}')
//                    speedDetector.vehicle = vehicle;
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ViolatorDetail(vehicle.deviceId)));
                      },
                    );
                  });
            }
          } else if (snapshot.hasError) {
            print('The error: ${snapshot.error}');
            return Center(
              child: Text('Error while getting list of violators!'),
            );
          }
          return Center(
            child: CircularProgressIndicator(),
          );
        });
  }
}
