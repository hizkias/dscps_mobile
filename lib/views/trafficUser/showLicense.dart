import 'package:dscps/models/License.dart';
import 'package:flutter/material.dart';
import 'package:dscps/utils/uiHelper.dart';

import 'package:dscps/services/apiService.dart';

class ShowLicense extends StatelessWidget{
  var licenseId;
  ShowLicense(this.licenseId);
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
          Navigator.pop(context);
        }),
        title: Text('License information'),
      ),
      body: FutureBuilder<License>(
          future: ApiService.getDriverLicense(this.licenseId),
          builder: (context, snapshot) {
            print('has data: ${snapshot.hasData}');
            print('has error: ${snapshot.hasError}');
            if (snapshot.hasData) {
              return Center(
                child: ListView(
                  children: <Widget>[
                    ListTile(
                      title: Text(
                        'License Information',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                    ListTile(
                      title: FadeInImage.assetNetwork(
                        width: MediaQuery.of(context).size.width / 5,
                        height:
                        MediaQuery.of(context).size.height / 5,
                        placeholder: 'assets/spinningwheel.gif',
                        image:
                        '${ApiService.licensepi}Containers/files/download/${snapshot.data.imageUrl}',
                      ),
                    ),
                    Divider(),
                    ListTile(
                      leading: Icon(Icons.person),
                      title: Text('Full Name'),
                      subtitle: Text('${snapshot.data.getFullName()}'),
                    ),
                    ListTile(
                      leading: Icon(Icons.wc),
                      title: Text('Gender'),
                      subtitle: Text('${snapshot.data.gender}'),
                    ),
                    ListTile(
                      leading: Icon(Icons.history),
                      title: Text('Age'),
                      subtitle: Text('${snapshot.data.age}'),
                    ),
                    ListTile(
                      leading: Icon(Icons.group_work),
                      title: Text('Blood group'),
                      subtitle: Text('${snapshot.data.bloodGroup}'),
                    ),
                    ListTile(
                      leading: Icon(Icons.insert_drive_file) ,
                      title: Text('License no'),
                      subtitle:  Text('${snapshot.data.licenseNo}'),
                    ),
                    ListTile(
                      leading: Icon(Icons.location_on),
                      title: Text('License Site'),
                      subtitle: Text('${snapshot.data.licenseSite}'),
                    ),
                  ],
                ),
              );
            } else if (snapshot.hasError) {
              return Center(
                child:
                Text('Error while getting license information'),
              );
            }
            return Center(
              child: CircularProgressIndicator(),
            );
          }),
    ));
  }

}