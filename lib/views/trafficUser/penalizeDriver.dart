import 'package:dscps/models/CrimeType.dart';
import 'package:dscps/models/PenalityRecord.dart';
import 'package:dscps/models/SpeedDetectorDevice.dart';
import 'package:dscps/models/SpeedLimitViolator.dart';
import 'package:dscps/utils/dbHelper.dart';
import 'package:flutter/material.dart';
import '../../utils/uiHelper.dart';
import '../../services/apiService.dart';
//import 'package:location/location.dart';

import 'dart:async';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';

class PenalizeDriver extends StatelessWidget {
//  final List<CrimeType> crimeTypes;
  SpeedDetectorDevice violator;
  PenalizeDriver() {}
  PenalizeDriver.fromViolatorDetailScreen(this.violator);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text('Penalize Driver'),
      ),
      body: this.violator == null
          ? FutureBuilder<List<CrimeType>>(
              future: ApiService.getCrimeTypes(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length == 0) {
                    return Center(
                      child: Text('No crime types found'),
                    );
                  } else {
                    return _PenalizeDriverForm(snapshot.data);
                  }
                } else if (snapshot.hasError) {
                  return Center(
                    child: Text('Error while getting crime tyes list'),
                  );
                }

                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            )
          : FutureBuilder<List<CrimeType>>(
              future: ApiService.getCrimeTypes(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length == 0) {
                    return Center(
                      child: Text('No crime types found'),
                    );
                  } else {
                    return _PenalizeDriverForm.fromViolator(
                        snapshot.data, violator);
                  }
                } else if (snapshot.hasError) {
                  return Center(
                    child: Text('Error while getting crime tyes list'),
                  );
                }

                return Center(
                  child: CircularProgressIndicator(),
                );
              },
            ),
    ));
  }
}

class _PenalizeDriverForm extends StatefulWidget {
  List<CrimeType> crimeTypes;
  SpeedDetectorDevice violator;
  _PenalizeDriverForm(this.crimeTypes);
  _PenalizeDriverForm.fromViolator(this.crimeTypes, this.violator);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return violator == null
        ? _PenalizeDriverFormState(this.crimeTypes)
        : _PenalizeDriverFormState.fromViolator(this.crimeTypes, this.violator);
  }
}

class _PenalizeDriverFormState extends State<_PenalizeDriverForm> {
  final _formKey = GlobalKey<FormState>();
  SpeedDetectorDevice violator;

  TextEditingController _licenseIdController = new TextEditingController();
  TextEditingController _noteOnIncidenceController =
      new TextEditingController();

  List<CrimeType> crimeTypes;
  List<DropdownMenuItem<CrimeType>> crimeTypesDropdownItems =
      List<DropdownMenuItem<CrimeType>>();
  bool isPressed = false;
  CrimeType selectedCrimeType;
  DatabaseHelper _dbHelper = new DatabaseHelper();
//  Location location = new Location();
  _PenalizeDriverFormState(this.crimeTypes) {
    print('the size of crimelist ${this.crimeTypes.length}');
    this.selectedCrimeType = this.crimeTypes[0];

    for (var i = 0; i < this.crimeTypes.length; i++) {
      crimeTypesDropdownItems.add(DropdownMenuItem<CrimeType>(
        value: this.crimeTypes[i],
        child: Text('${this.crimeTypes[i].name}'),
      ));
    }
  }

  _PenalizeDriverFormState.fromViolator(this.crimeTypes, this.violator) {
    this.selectedCrimeType = this.crimeTypes[0];

    for (var i = 0; i < this.crimeTypes.length; i++) {
      crimeTypesDropdownItems.add(DropdownMenuItem<CrimeType>(
        value: this.crimeTypes[i],
        child: Text('${this.crimeTypes[i].name}'),
      ));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
        key: _formKey,
        child: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.insert_drive_file),
              title: TextFormField(
                controller: _licenseIdController,
                decoration: InputDecoration(
                    hintText: 'License Id',
                    suffix: IconButton(
                        icon: Icon(Icons.camera_alt), onPressed: scan)),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'License Id is required';
                  }
                },
              ),
            ),
            ListTile(
                leading: Icon(Icons.message),
                title: TextFormField(
                    controller: _noteOnIncidenceController,
                    decoration: InputDecoration(hintText: 'Note on incident'),
                    maxLines: 7,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'Note on incident is required';
                      }
                    })),
            ListTile(
              leading: Icon(Icons.category),
              title: DropdownButton<CrimeType>(
                isExpanded: true,
                items: this.crimeTypesDropdownItems,
                onChanged: (value) {
                  setState(() {
                    this.selectedCrimeType = value;
                  });
                },
                value: this.selectedCrimeType,
                elevation: 2,
//                style: TextStyle( fontSize: 30),
                isDense: true,
                iconSize: 40.0,
              ),
//              subtitle: Text(''),
            ),
            ListTile(
              title: RaisedButton.icon(
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
//                      print('violator is null ${violator == null}');
//                      print('crimeTypes is null ${crimeTypes == null}');
//                      print(
//                          'selected crime type name ${selectedCrimeType.name}');
                      var user = await _dbHelper.getUser();
                      var penalityRecord = PenalityRecord(
                          driverLicenseId: _licenseIdController.text,
                          noteOnIncidence: _noteOnIncidenceController.text,
                          timeOfIncidence: DateTime.now(),
                          lat: '',
                          lng: '',
                          crimeTypeCategory:
                              violator != null ? 'speed' : 'other',
                          speedZoneId: user.speedZoneId,
                          deviceId: violator != null ? violator.id : '',
                          useraccountId: user.userId,
                          crimeTypeId: selectedCrimeType.id);
                      var successfullyCreated = false;
                      if (violator == null) {
                        PenalityRecord result =
                            await ApiService.createPenalityRecord(
                                penalityRecord.toMapForNonSpeedViolator());
                        if (result != null) {
                          successfullyCreated = true;
                        } else {
                          successfullyCreated = false;
                        }
                      } else {
                        PenalityRecord result =
                            await ApiService.createPenalityRecord(
                                penalityRecord.toMapForSpeedViolator());
                        if (result != null) {
                          successfullyCreated = true;
                        } else {
                          successfullyCreated = false;
                        }
                      }

                      if (successfullyCreated) {
                        Scaffold.of(context).showSnackBar(SnackBar(
                            content:
                                Text('Succesfully created penalty record')));
                      }
                    }
                  },
                  icon: Icon(Icons.done),
                  label: Text('Submit')),
            )
          ],
        ));
  }

  Future scan() async {
    try {
      String barcode = await BarcodeScanner.scan();
      setState(() => this._licenseIdController.text = barcode);
//      print('The result is ${this.licenseId}');
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
//        setState(() {
////          this.licenseId = 'The user did not grant the camera permission!';
//        });
      } else {
//        setState(() => this.licenseId = 'Unknown error: $e');
      }
    } on FormatException {
//      setState(() => this.licenseId =
//      'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
//      setState(() => this.licenseId = 'Unknown error: $e');
    }
  }
}
