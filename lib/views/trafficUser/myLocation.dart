import 'package:flutter/material.dart';
import '../../utils/uiHelper.dart';

class MyLocation extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return UiHelper.createComponent(Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            }),
        title: Text('My current location'),
      ),
      body: Container(
        margin: EdgeInsets.only(left: 5, right: 5),
        padding: EdgeInsets.only(top: 10),
        child: Card(
          child: Row(

            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[Icon(Icons.my_location)],
                ),
              ),

              Container(
                margin: EdgeInsets.only(left: 20),
                child:   Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,

                  children: <Widget>[
                    Row(

                      children: <Widget>[
                        Text('Zone Id')
                      ],
                    ), Row(
                      children: <Widget>[
                        Text('Name of Location')
                      ],
                    ), Row(
                      children: <Widget>[
                        Text('Max.Speed')
                      ],
                    ), Row(
                      children: <Widget>[
                        Text('Zone Id')
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(left: 30),
                child:  Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(

                      children: <Widget>[
                        Text('DSCPSAA0909')
                      ],
                    ), Row(
                      children: <Widget>[
                        Text('Arat Kilo')
                      ],
                    ), Row(
                      children: <Widget>[
                        Text('40km/hr')
                      ],
                    ), Row(
                      children: <Widget>[
                        Text('Zone Id')
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    ));
  }
}
