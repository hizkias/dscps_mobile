import 'package:flutter/material.dart';
import '../../utils/uiHelper.dart';

class Settings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: (){
              Navigator.pop(context);
            }
            ),
        title: Text('Settings'),
      ),
      body: ListView(
        children: <Widget>[
          Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.settings),
                  title: Text('Change account settings'),
                  subtitle: Text('username, password'),
                ),
              ],
            ),
          ),   Card(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const ListTile(
                  leading: Icon(Icons.cached),
                  title: Text('Cache setting'),
                  subtitle: Text('When and where to store cached...'),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

}