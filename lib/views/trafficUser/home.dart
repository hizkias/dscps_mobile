import 'package:dscps/models/SpeedZone.dart';
import 'package:dscps/models/User.dart';
import 'package:dscps/views/driverUser/penalities.dart';
import 'package:flutter/material.dart';
import '../../utils/uiHelper.dart';
import 'dart:io';

import '../login.dart';
import './myLocation.dart';
import './speedLimitViolatorsList.dart';
import './penalizeDriver.dart';
import './settings.dart';
import './scanLicense.dart';
import '../../services/apiService.dart';
import '../../utils/dbHelper.dart';
import 'package:http/http.dart' as http;
import '../../services/apiService.dart';
import 'dart:isolate';

class TrafficHome extends StatelessWidget {
  User user;
  var ctx;
  TrafficHome(this.user) {}

  @override
  Widget build(BuildContext context) {
    this.ctx = context;
    // TODO: implement build
    return UiHelper.createComponent(
      Scaffold(
        appBar: AppBar(
          leading: _ScaffoldLeading(),
          title: Text('DSCPS'),
          centerTitle: true,
        ),
        drawer: _DrawerComponent(user),
        body: _HomeBody(user),
      ),
    );
  }

  static void toggleDrawer(context) {
    Scaffold.of(context).openDrawer();
  }
}

class _HomeBody extends StatefulWidget {
  var user;

  _HomeBody(this.user) {}
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomeBodyState(this.user);
  }
}

class _HomeBodyState extends State<_HomeBody> {
  User user;
  Isolate isolate;
  _HomeBodyState(this.user) {}
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView(
      children: <Widget>[
        Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              ListTile(
                leading: Icon(Icons.person),
                // ignore: const_eval_throws_exception, invalid_constant
                title: Text('${user.firstname} ${user.middlename}'),
                subtitle: Text('Road traffic officer on duty'),
              ),
            ],
          ),
        ),
        FutureBuilder<SpeedZone>(
          future: ApiService.getSpeedZone(this.user.speedZoneId),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            print('snapshot has data ${snapshot.data}');
            if(snapshot.hasData){
              return Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                     ListTile(
                      leading: Icon(Icons.location_on),
                      title: Text('Speedzone Assigned'),
                      subtitle: Text('${snapshot.data.name}'),
                    ),
                  ],
                ),
              );
            }
            else if(snapshot.hasError){
              return Card(
                child: ListTile(
                  leading: Icon(Icons.location_on),
                  title: Text('Speedzone Assigned'),
                  subtitle: Text('Not assigned yet'),
                ),
              );
            }
            return Card(
              child: CircularProgressIndicator(),
            );
          },
        ),
//        Card(
//          child: Column(
//            mainAxisSize: MainAxisSize.min,
//            children: <Widget>[
//              const ListTile(
//                leading: Icon(Icons.report),
//                title: Text('Incident summary'),
//                subtitle: Text('No incidents'),
//              ),
//            ],
//          ),
//        ),
//        Card(
//          child: Column(
//            mainAxisSize: MainAxisSize.min,
//            children: <Widget>[
//               ListTile(
//                leading: Icon(Icons.report),
//                title: RaisedButton(onPressed: ()  {   this.start();}, child: Text('Start isolate'),),
//                subtitle: RaisedButton(onPressed: (){ this.stop(); }, child:  Text('Stop isolate'),),
//              ),
//            ],
//          ),
//        ),
//        ListTile(
//          title: RaisedButton(onPressed: () {ApiService.getImage();}, child: Text('load image'),),
//              )
      ],
    );
    ;
  }
  void start() async {
    ReceivePort receivePort = ReceivePort();
    print('about to start the isolate');
    isolate =  await Isolate.spawn(_HomeBodyState.doPeriodically, receivePort.sendPort);
  print('started the isolate process');
    receivePort.listen((msg){
      print(msg);
    });

  }

  static void doPeriodically(SendPort sendPort){
    print('about to start the function in isolate');
    int counter = 0;
    while(true){
      counter++;
      String msg = 'notification ${counter}';
      stdout.write('SEND: ${msg}');
      sendPort.send(msg);
      sleep(Duration(seconds: 2));
    }
  }

  void stop(){
    print('about to kill isolate');
    print('is isolate existing? ${isolate == null}');
    if(this.isolate != null){
      stdout.writeln('killing isolate...');
      isolate.kill(priority: Isolate.immediate);
      isolate = null;
    }
  }
}

class _ScaffoldLeading extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(Icons.menu),
      onPressed: () {
        TrafficHome.toggleDrawer(context);
      },
    );
  }
}

class _DrawerComponent extends StatelessWidget {
  User user;
  _DrawerComponent(this.user) {}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            currentAccountPicture: CircleAvatar(
              child: Text(
                  '${user.firstname[0].toUpperCase()}${user.middlename[0].toUpperCase()}'),
            ),
            accountEmail: Text('${user.email}'),
            accountName: Text('${user.firstname} ${user.middlename}'),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => TrafficHome(this.user)));
            },
          ),
          ListTile(
            leading: Icon(Icons.warning),
            title: Text('Speed violators'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SpeedViolators()));
            },
          ),
          ListTile(
            leading: Icon(Icons.announcement),
            title: Text('Penalize driver'),
            onTap: () async{
//              var crimeTypes = await ApiService.getCrimeTypes();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PenalizeDriver()));
            },
          ),  ListTile(
            leading: Icon(Icons.history),
            title: Text('Penalities'),
            onTap: () async{
//              var crimeTypes = await ApiService.getCrimeTypes();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => PenaltiesList()));
            },
          ),

          ListTile(
            leading: Icon(Icons.scanner),
            title: Text('Scan license'),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ScanLicense()));
            },
          ),
//          ListTile(
//            leading: Icon(Icons.settings),
//            title: Text('Settings'),
//            onTap: () {
//              Navigator.push(
//                  context, MaterialPageRoute(builder: (context) => Settings()));
//            },
//          ),
          ListTile(
            leading: Icon(Icons.lock),
            title: Text('Logout'),
            onTap: () async {
              var logoutResponse = await ApiService.processLogout();
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginComponent()));
            },
          ),
        ],
      ),
    );
  }
}
