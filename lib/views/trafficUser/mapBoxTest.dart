import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';

class MapboxTest extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MapboxTestState();
  }
}

class _MapboxTestState extends State<MapboxTest> {
  dynamic currentLat = 9.033631439468422;
  dynamic currentLng = 38.76315133476112;
  dynamic currentZoom = 0.0;
  MapController mapController = new MapController();
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Mapbox test'),
        ),
        body: FlutterMap(
            mapController: mapController,
            options: new MapOptions(
                center: new LatLng(currentLat, currentLng),
                minZoom: 5.0,
                zoom: currentZoom),
            layers: [
              TileLayerOptions(
                  urlTemplate:
                      'https://api.mapbox.com/styles/v1/hizkias/cjvtw3z5n0au11clvgjrtpybt/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiaGl6a2lhcyIsImEiOiJjanYzb2dweWcyaHFoM3lsaml0a3lhcmhzIn0.nG_CTyu-otJU2VC5cnZ-ZQ',
                  additionalOptions: {
                    'accessToken':
                        'pk.eyJ1IjoiaGl6a2lhcyIsImEiOiJjanYzb2dweWcyaHFoM3lsaml0a3lhcmhzIn0.nG_CTyu-otJU2VC5cnZ-ZQ',
                    'id': 'mapbox.mapbox-streets-v8'
                  }),
              new MarkerLayerOptions(markers: [
                new Marker(
                    width: 45.0,
                    height: 45.0,
                    point: new LatLng(currentLat, currentLng),
                    builder: (context) => new Container(
                          child: IconButton(
                            icon: Icon(Icons.location_on),
                            color: Colors.blue,
                            iconSize: 45.0,
                            onPressed: () {
                              print('Marker tapped');
                            },
                          ),
                        ))
              ])
            ]),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
            onPressed: () {
            mapController.move(mapController.center, mapController.zoom + 5);

        }),
      ),
    );
  }
}
