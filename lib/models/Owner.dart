class Owner {
  String name;
  String address;
  String phoneNumber;

  Owner({this.name, this.address, this.phoneNumber}){}

  factory Owner.fromJSON(Map<String, dynamic> data){
    return Owner(
      name: data['name'],
      address: data['address'],
      phoneNumber: data['phoneNumber']
    );
  }
}