class SpeedZone {
  String name;
  dynamic speedLimit;
  dynamic lat;
  dynamic lng;
  dynamic distanceCovered;
  String id;

  SpeedZone(
      {this.name,
      this.speedLimit,
      this.lat,
      this.lng,
      this.distanceCovered,
      this.id});

  factory SpeedZone.fromJson(Map<String, dynamic> data) {
    return SpeedZone(
        name: data['name'],
        speedLimit: data['speedLimit'],
        lat: data['lat'],
        lng: data['lng'],
        distanceCovered: data['distanceCovered'],
        id: data['id']);
  }

  toMap() {
    return {
      'name': name,
      'speedLimit': speedLimit,
      'lat': lat,
      'lng': lng,
      'distanceCovered': distanceCovered,
      'id': id
    };
  }
}
