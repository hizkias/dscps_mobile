import 'CrimeType.dart';
import 'SpeedZone.dart';
import 'User.dart';
import 'Vehicle.dart';

class PenalityRecord {
  String id;
  String driverLicenseId;
  String noteOnIncidence;
  DateTime timeOfIncidence;
  CrimeType crimeType;
  String crimeTypeId;
  String crimeTypeCategory;
  String speedZoneId;
  dynamic lat;
  dynamic lng;
  String useraccountId;
  String deviceId;
  Vehicle vehicle;
  SpeedZone speedZone;
  String referenceNumber;
  String trafficFullname;
  User traffic;
  bool isResolved;

  PenalityRecord(
      {this.id,
      this.driverLicenseId,
      this.noteOnIncidence,
      this.timeOfIncidence,
      this.crimeTypeId,
      this.lat,
      this.lng,
      this.speedZoneId,
      this.crimeTypeCategory,
      this.deviceId,
      this.useraccountId,
        this.trafficFullname,
      this.traffic,
      this.referenceNumber,
      this.isResolved});

  factory PenalityRecord.fromJson(Map<String, dynamic> data) {
    return PenalityRecord(
        id: data['id'],
        driverLicenseId: data['driverLicenseId'],
        noteOnIncidence: data['noteOnIncidence'],
        timeOfIncidence: DateTime.parse(data['timeOfIncidence']),
        crimeTypeId: '',
        lat: data['lat'],
        lng: data['lng'],
        referenceNumber: data['referenceNumber'],
        speedZoneId: data['speedZoneId'] != null ? data['speedZoneId'] : '',
        deviceId: data['vehicleId'] != null ? data['vehicleId'] : '',
        useraccountId:
            data['useraccountId'] != null ? data['useraccountId'] : '',
        isResolved: data['isResolved']);
  }

  toMap() {
    return {
      'driverLicenseId': '${driverLicenseId}',
      'noteOnIncidence': '${noteOnIncidence}',
      'timeOfIncidence': '${timeOfIncidence.toString()}',
      'isResolved': '${isResolved}',
      'crime-typeId': '${crimeTypeId}',
      'lat': '${lat}',
      'lng': '${lng}',
      'useraccountId': '${useraccountId}',
      'speedZoneId': '${speedZoneId}',
      'deviceId': '${deviceId}'
          'crimeCategory'
    };
  }

  toMapForNonSpeedViolator() {
    return {
      'driverLicenseId': '${driverLicenseId}',
      'noteOnIncidence': '${noteOnIncidence}',
      'timeOfIncidence': '${timeOfIncidence.toString()}',
      'crime-typeId': '${crimeTypeId}',
      'crimeTypeCategory': '${crimeTypeCategory}',
      'lat': '${lat}',
      'lng': '${lng}',
      'useraccountId': '${useraccountId}',
      'speedZoneId': '${speedZoneId}'
    };
  }

  toMapForSpeedViolator() {
    return {
      'driverLicenseId': '${driverLicenseId}',
      'noteOnIncidence': '${noteOnIncidence}',
      'timeOfIncidence': '${timeOfIncidence.toString()}',
      'crime-typeId': '${crimeTypeId}',
      'lat': '${lat}',
      'lng': '${lng}',
      'useraccountId': '${useraccountId}',
      'speedZoneId': '${speedZoneId}',
      'deviceId': '${deviceId}',
      'crimeTypeCategory': '${crimeTypeCategory}'
    };
  }
}
