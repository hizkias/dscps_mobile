import 'SpeedDetectorDevice.dart';
import 'SpeedZone.dart';


class SpeedLimitViolator{
  dynamic speedDetected;
  DateTime timeOfViolation;
  SpeedDetectorDevice speedDetectorDevice;
  SpeedZone speedZone;
  dynamic lat;
  dynamic lng;
  String id;

  SpeedLimitViolator({this.speedDetected, this.timeOfViolation, this.lat, this.lng, this.id});

  factory SpeedLimitViolator.fromJson(Map<String, dynamic> data){

    return SpeedLimitViolator(
      id: data['id'],
      timeOfViolation: DateTime.parse(data['timeOfViolatioin']),
//      speedDetectorDevice: SpeedDetectorDevice(),
//      speedZone: SpeedZone(),
      lat: data['lat'],
      lng: data['lng'],
      speedDetected: data['speedDetected']
    );
  }
}

