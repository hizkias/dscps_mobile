class License {
  String firstName;
  String middleName;
  String lastName;
  String gender;
  dynamic age;
  String bloodGroup;
  String licenseNo;
  String licenseCategory;
  String licenseSite;
  String town;
  String imageUrl;

  License({
    this.firstName,
    this.middleName,
    this.lastName,
    this.gender,
    this.age,
    this.bloodGroup,
    this.licenseNo,
    this.licenseCategory,
    this.licenseSite,
    this.town,
    this.imageUrl
});

  factory License.fromJson(Map<String, dynamic> data) {
    return License(
      firstName: data['firstName'],
      middleName: data['middleName'],
      lastName: data['lastName'],
      gender: data['gender'],
      age: data['age'],
      bloodGroup: data['bloodGroup'],
      licenseNo: data['licenseNo'],
      licenseCategory: data['licenseCategory'],
      licenseSite: data['licenseSite'],
      town: data['town'],
      imageUrl: data['imageUrl']
    );
  }

  String getFullName(){
    return '${firstName} ${middleName} ${lastName}';
  }

  toMap(){
    return {
      'firstName': firstName,
      'middleName': middleName,
      'lastName': lastName,
      'gender': gender,
      'bloodGroup': bloodGroup,
      'age': age,
      'licenseNo': licenseNo,
      'licenseCategory': licenseCategory,
      'licenseSite': licenseSite,
      'town': town,
      'imageUrl': imageUrl
    };
  }

}
