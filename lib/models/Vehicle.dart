import 'Owner.dart';
import 'dart:convert';

class Vehicle {
  String vehicleType;
  String model;
  String plateNo;
  String year;
  Owner owner;
  String deviceId;
  String id;

  Vehicle(
      {this.vehicleType,
      this.model,
      this.plateNo,
      this.year,
      this.owner,
      this.deviceId,
      this.id});

  factory Vehicle.fromJson(Map<String, dynamic> data) {
    return Vehicle(
        vehicleType: data['vehicleType'],
        model: data['model'],
        plateNo: data['plateNo'],
        year: data['year'],
        owner: Owner(),
        id: data['id']);
  }
}
