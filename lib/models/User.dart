import 'SpeedZone.dart';

class User {
  String firstname;
  String middlename;
  String lastname;
  String email;
  String username;
  String userType;
  String accessToken;
  SpeedZone speedZone;
  String speedZoneId;
  String userId;
  String driverLicenseId;

  User({this.firstname, this.middlename, this.lastname, this.email, this.username, this.userType, this.accessToken, this.userId, this.speedZoneId, this.driverLicenseId});

  factory User.fromJson(Map<String, dynamic> data, [fromApi = true]){
    var userData = fromApi ? data['user'] : data;
    var token = fromApi ? data['id'] : userData['accessToken'];
    return User(
      firstname: userData['firstname'],
      middlename: userData['middlename'],
      lastname: userData['lastname'],
      email: userData['email'],
      username: userData['username'],
      userType: userData['userType'],
      speedZoneId: userData['speedZoneId'],
      driverLicenseId: userData['driverLicenseId'],
      accessToken: token,
      userId: data['userId']
    );
  }

  toMap(){
    return {
      'firstname': firstname,
      'middlename': middlename,
      'lastname': lastname,
      'email': email,
      'username': username,
      'userType': userType,
      'driverLicenseId': driverLicenseId,
      'speedZoneId': speedZoneId,
      'accessToken': accessToken,
      'userId': userId
    };
  }
}