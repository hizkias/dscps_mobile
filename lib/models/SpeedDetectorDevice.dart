import 'SpeedLimitViolator.dart';
import 'Vehicle.dart';


class SpeedDetectorDevice{
  String id;
  String currentSpeedZoneId;
  dynamic lat;
  dynamic lng;
  Vehicle vehicle;
  List<SpeedLimitViolator> speedViolations;

  SpeedDetectorDevice({this.id, this.vehicle, this.currentSpeedZoneId, this.lat, this.lng}){}

  factory SpeedDetectorDevice.fromJson(Map<String, dynamic> data){
    return SpeedDetectorDevice(
      id: data['id'],
      vehicle: Vehicle(),
      currentSpeedZoneId: data['currentSpeedZoneId'],
      lat: data['lat'],
      lng: data['lng']
    );
  }
}