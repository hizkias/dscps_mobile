class CrimeType{
  String name;
  String description;
  String riskLevel;
  dynamic fineAmount;
  String id;

  CrimeType({this.name, this.description, this.riskLevel, this.fineAmount, this.id});

  factory CrimeType.fromJson(Map<String, dynamic> data){
      return CrimeType(
        name: data['name'],
        description: data['description'],
        riskLevel: data['reskLevl'],
        fineAmount: data['fineAmount'],
        id: data['id']
      );
  }
}