import 'package:flutter/material.dart';
import './views/login.dart';

import './utils/dbHelper.dart';
import './services/apiService.dart';
import './views/driverUser/home.dart';
import './views/trafficUser/home.dart';

void main() async {
  final dbHelper = DatabaseHelper();
  print('Checking login...');
  bool loginStatus = await ApiService.isLoggedIn();

  print('is LoggedIn: ${loginStatus}');

  if (loginStatus) {
    var user = await dbHelper.getUser();
    if (user.userType == 'Driver') {
      var license = await dbHelper.getDriverLicense();
      runApp(DriverHome(user, license));
    }
    else if(user.userType == 'Traffic_police'){
      runApp(TrafficHome(user));
    }
  } else {
    runApp(LoginComponent());
  }
}
