import 'dart:async';

import 'package:dscps/models/CrimeType.dart';
import 'package:dscps/models/License.dart';
import 'package:dscps/models/Owner.dart';
import 'package:dscps/models/PenalityRecord.dart';
import 'package:dscps/models/SpeedDetectorDevice.dart';
import 'package:dscps/models/SpeedLimitViolator.dart';
import 'package:dscps/models/SpeedZone.dart';
import 'package:dscps/models/Vehicle.dart';
import 'package:http/http.dart' as http;
import '../utils/dbHelper.dart';
import '../models/User.dart';
import 'dart:convert';

class ApiService {
  static String hostIp = '10.0.2.2';
  static final _dbHelper = DatabaseHelper();
  static String _apiRoot = 'http://${hostIp}:3000/api/';
  static String licensepi = 'http://${hostIp}:3100/api/';

  // Logs in user
  // ignore: missing_return
  static Future<User> processLogin(credentials) async {
    http.Response resp = await http
        .post(_apiRoot + 'useraccounts/login?include=user', body: credentials);

    if (resp.statusCode == 200) {
      print(resp.body);
      return User.fromJson(jsonDecode(resp.body));
    } else if (resp.statusCode == 401) {
      throw Exception('unauthorized');
    } else {
      throw Exception('network');
    }
  }

  // Logs out user
  static Future<int> processLogout() async {
    var result = await _dbHelper.deleteUsers();
    await _dbHelper.deleteLicenses();
    return result;
  }

  // Checks if a user is logged in
  static Future<bool> isLoggedIn() async {
    User user = await _dbHelper.getUser();

    if (user != null) {
      return true;
    } else {
      return false;
    }
  }

  // Retrieves list of speed limit violated to be modified to query only the nearest)
  static Future<List<Vehicle>> getSpeedLimitViolatiors() async {
    var user = await _dbHelper.getUser();
  print('filter={"where":{"and":[{"speedZoneId":"${user.speedZoneId}"}, {"didCurrentlyViolateSeedLimt": true} ]},"include":["vehicle"]}');
    var rawResponse = await http.get(
        '${_apiRoot}speed-detector-devices?access_token=${user.accessToken}&filter={"where":{"and":[{"speedZoneId":"${user.speedZoneId}"}, {"didCurrentlyViolateSeedLimt": true} ]},"include":["vehicle"]}');

    if (rawResponse.statusCode == 200) {
      dynamic violatorsMap = jsonDecode(rawResponse.body);
      print("This is the response ${rawResponse.body}");
      List<Vehicle> speedViolators = List<Vehicle>();

      if (violatorsMap.length > 0) {
        for (var i = 0; i < violatorsMap.length; i++) {
//          print(violatorsMap[i]);
          var owner = Owner.fromJSON(
              violatorsMap[i]['vehicle']['owner']);

          var vehicle = Vehicle.fromJson(
              violatorsMap[i]['vehicle']);
          vehicle.owner = owner;
          vehicle.deviceId = violatorsMap[i]['id'];
          print('speed violator device id ${vehicle.deviceId}');

          speedViolators.add(vehicle);
        }
      }
      return speedViolators;
    } else if (rawResponse.statusCode == 401) {
      throw Exception('unauthorized');
    } else {
      throw Exception('other errors');
    }
  }
  // Gets a single violator to track
  static Future<SpeedDetectorDevice> getSpeedViolator(id) async{
    var violatorResult = await http.get('${_apiRoot}speed-detector-devices/${id}?filter={"include":[{"speed-limit-violators":"speedZone"}, "vehicle"]}');
//    print('the violator object is ${violatorResult.body}');
    if(violatorResult.statusCode == 200){
      print('The response here again ${violatorResult.body}');
      List<SpeedLimitViolator> speedViolators = new List<SpeedLimitViolator>();

      var violatorJSON = jsonDecode(violatorResult.body);
      var theDetectorDevice = SpeedDetectorDevice.fromJson(violatorJSON);

      var speedViolations = violatorJSON['speed-limit-violators'];
      for(var i = 0; i < speedViolations.length; i++){
        var iSpeedZone = SpeedZone.fromJson(speedViolations[i]['speedZone']);
        var iViolation = SpeedLimitViolator.fromJson(speedViolations[i]);
        iViolation.speedZone = iSpeedZone;
        speedViolators.add(iViolation);
      }

      theDetectorDevice.vehicle = Vehicle.fromJson(violatorJSON['vehicle']);
      theDetectorDevice.speedViolations = speedViolators;

      return theDetectorDevice;
    }
    else {
      return null;
    }

  }

  static Future<SpeedZone> getSpeedZone(id) async {
    var response = await http.get('${_apiRoot}speedZones/${id}');
//    print(response.body);
    if (response.statusCode == 200) {
      return SpeedZone.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('has error');
    }
  }

  // Gets list of crime types
  static Future<List<CrimeType>> getCrimeTypes() async {
    http.Response crimesResponse = await http.get('${_apiRoot}crime-types');

    if (crimesResponse.statusCode == 200) {
      List<CrimeType> crimeTypesList = List<CrimeType>();

      var rawList = jsonDecode(crimesResponse.body);

      for (int i = 0; i < rawList.length; i++) {
        crimeTypesList.add(CrimeType.fromJson(rawList[i]));
      }

      return crimeTypesList;
    } else {
      throw Exception('Error occured while getting list of crime types');
    }
  }

  // creating of new penality record
  static Future<PenalityRecord> createPenalityRecord(
      newPenality) async {
    print('inside the api method');
//    print({'test': newPenality.driverLicenseId});

    var createResponse = await http.post('${_apiRoot}penality-records',
        body: newPenality);

    if (createResponse.statusCode == 200) {
      return PenalityRecord.fromJson(jsonDecode(createResponse.body));
    } else {
      throw Exception('error while creating penality record');
    }
  }

  // for driver user to get list of records by

  static Future<List<PenalityRecord>> getMyPenalityRecords() async {
    var user = await _dbHelper.getUser();

    http.Response getListResponse = await http.get(
        '${_apiRoot}/penality-records?filter={"where":{"driverLicenseId":"${user.driverLicenseId}"}, "include":["crime-type", "useraccount"]}');
    print('The driver id: ${user.driverLicenseId}');
    print('The response ${getListResponse.body}');

    if (getListResponse.statusCode == 200) {
      var rawList = jsonDecode(getListResponse.body);
      List<PenalityRecord> records = List<PenalityRecord>();

      for (int i = 0; i < rawList.length; i++) {
        var penalityRecord = PenalityRecord.fromJson(rawList[i]);
        var crimeType = CrimeType.fromJson(rawList[i]['crime-type']);
        print('The useraccount is ${rawList[i]['useraccount']['firstname']}');
//        var trafficUser = User.fromJson(rawList[i]['useraccount']);
        penalityRecord.crimeType = crimeType;
        penalityRecord.trafficFullname  = rawList[i]['useraccount']['firstname'] +' '+ rawList[i]['useraccount']['middlename'];
//        penalityRecord.traffic = trafficUser;
        records.add(penalityRecord);
      }

      return records;
    } else {
      throw Exception('Error occured');
    }
  }

  static Future<License> getDriverLicense(licenseId) async {
    print('Inside of get driver license');
    http.Response licenseResponse = await http.get(
        '${licensepi}licenses?filter={"where":{"licenseNo":"${licenseId}"}}');
//    print(licenseResponse.body);

    if (licenseResponse.statusCode == 200) {
      print('driver found');
      return License.fromJson(jsonDecode(licenseResponse.body)[0]);
    } else {
      throw Exception('Error while');
    }
  }

  static Future<List<PenalityRecord>> getPenalties() async{
      var user = await _dbHelper.getUser();
      List<PenalityRecord> records = new List<PenalityRecord>();

      var response = await http.get('${_apiRoot}useraccounts/${user.userId}/penality-records?filter={"include": ["crime-type"]}');
      if(response.statusCode == 200){
          var rawList = jsonDecode(response.body);
          for(var i = 0; i < rawList.length; i++){
            var penalityRecord = PenalityRecord.fromJson(rawList[i]);
            var crimeType = CrimeType.fromJson(rawList[i]['crime-type']);
            penalityRecord.crimeType = crimeType;
            records.add(penalityRecord);
          }

          return records;
      }

      else {
        throw Exception('Error while getting penalities list');
      }

  }


  static Future<http.Response> resetEmail(email) async{
    var response = await http.post('${_apiRoot}useraccounts/reset', body: email);

    return response;

  }



  // updates the location of a speed limit violator so as to track it for
  // the purpose of penality

  static Future<SpeedLimitViolator> trackViolatorLocation(
      violatorId, speedZoneId) async {
    var response = await http.get(
        '${_apiRoot}speed-limit-violators?filter={"where":{"and": [{"id":"${violatorId}"}, {"speedZoneId":"${speedZoneId}"}]}, "include": ["vehicle"]}');
    if (response.statusCode == 200) {
      return SpeedLimitViolator.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Error while getting driver location');
    }
  }
}
